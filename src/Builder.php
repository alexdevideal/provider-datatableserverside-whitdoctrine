<?php

namespace Doctrine\DataTables;

use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\QueryBuilder as ORMQueryBuilder;

/**
 * Class Builder
 * @package Doctrine\DataTables
 */
class Builder
{
    /**
     * @var array
     */
    protected $columnAliases = array();
    /**
     * @var string
     */
    protected $columnField = 'name'; // or 'data'
    /**
     * @var string
     */
    protected $indexColumn = '*';
    /**
     * @var QueryBuilder|ORMQueryBuilder
     */
    protected $queryBuilder;
    /**
     * @var array
     */
    protected $requestParams;

    /**
     * @return array
     */
    public function getData()
    {
        $query = $this->getFilteredQuery();

        // Order
        if (array_key_exists('iSortCol_0', $this->requestParams)) {
            for ($i = 0; $i < 1; $i++) {
                if (array_key_exists($this->requestParams['mDataProp_' . $i], $this->columnAliases)) {
                    $this->requestParams['mDataProp_' . $i] = $this->columnAliases[$this->requestParams['mDataProp_' . $i]];
                }
                $query->addOrderBy($this->requestParams['mDataProp_' . $i], $this->requestParams['sSortDir_0']);
            }
        }

        // Offset
        if (array_key_exists('iDisplayStart', $this->requestParams)) {
            $query->setFirstResult(intval($this->requestParams['iDisplayStart']));
        }
        // Limit
        if (array_key_exists('iDisplayLength', $this->requestParams)) {
            $length = intval($this->requestParams['iDisplayLength']);
            if ($length > 0) {
                $query->setMaxResults($length);
            }
        }
        // Fetch
        return $query instanceof ORMQueryBuilder ?
            $query->getQuery()->getScalarResult() : $query->execute()->fetchAll();
    }

    /**
     * @return QueryBuilder|ORMQueryBuilder
     */
    public function getFilteredQuery()
    {
        $query = clone $this->queryBuilder;
        $colunmsCount = (int)$this->requestParams['iColumns'];
        // Search
        if (!array_key_exists('sSearch', $this->requestParams)) {
            return $query;
        }

        $value = trim($this->requestParams['sSearch']);

        $orX = $query->expr()->orX();
        for ($i = 0; $i < $colunmsCount; $i++) {
            if ($this->requestParams['bSearchable_' . $i] == 'true') {
                if (array_key_exists($this->requestParams['mDataProp_' . $i], $this->columnAliases)) {
                    $this->requestParams['mDataProp_' . $i] = $this->columnAliases[$this->requestParams['mDataProp_' . $i]];
                }
                $orX->add($query->expr()->like($this->requestParams['mDataProp_' . $i], ':sSearch'));
            }
        }
        if ($orX->count() >= 1) {
            $query->andWhere($orX)
                ->setParameter('sSearch', "%{$value}%");
        }


        // Done
        return $query;
    }

    /**
     * @return int
     */
    public function getRecordsFiltered()
    {
        $query = $this->getFilteredQuery();
        if ($query instanceof ORMQueryBuilder) {
            return $query->resetDQLPart('select')
                ->select("COUNT({$this->indexColumn})")
                ->getQuery()
                ->getSingleScalarResult();
        } else {
            return $query->resetQueryPart('select')
                ->select("COUNT({$this->indexColumn})")
                ->execute()
                ->fetchColumn(0);
        }
    }

    /**
     * @return int
     */
    public function getRecordsTotal()
    {
        $query = clone $this->queryBuilder;
        if ($query instanceof ORMQueryBuilder) {
            return $query->resetDQLPart('select')
                ->select("COUNT({$this->indexColumn})")
                ->getQuery()
                ->getSingleScalarResult();
        }
        return $query->resetQueryPart('select')
            ->select("COUNT({$this->indexColumn})")
            ->execute()
            ->fetchColumn(0);

    }

    /**
     * @return array
     */
    public function getResponse()
    {
        if (!isset($this->requestParams['sEcho'])) {
            $this->requestParams['sEcho'] = 1;
        }
        return array(
            'aaData' => $this->getData(),
            'sEcho' => $this->requestParams['sEcho'],
            'iTotalDisplayRecords' => $this->getRecordsFiltered(),
            'iTotalRecords' => $this->getRecordsTotal(),
        );
    }


    /**
     * @param string $indexColumn
     * @return static
     */
    public function withIndexColumn($indexColumn)
    {
        $this->indexColumn = $indexColumn;
        return $this;
    }

    /**
     * @param array $columnAliases
     * @return static
     */
    public function withColumnAliases($columnAliases)
    {
        $this->columnAliases = $columnAliases;
        return $this;
    }

    /**
     * @param string $columnField
     * @return static
     */
    public function withColumnField($columnField)
    {
        $this->columnField = $columnField;
        return $this;
    }

    /**
     * @param QueryBuilder|ORMQueryBuilder $queryBuilder
     * @return static
     */
    public function withQueryBuilder($queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;
        return $this;
    }

    /**
     * @param array $requestParams
     * @return static
     */
    public function withRequestParams($requestParams)
    {
        $this->requestParams = $requestParams;
        return $this;
    }
}